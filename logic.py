#initiation variable
f = 'Frontend'
b = 'Backend'
n = 50

#create list for collect the data
collectData = []

#create loop until n
for i in range(1,n):
    if (i%3==0):
        if (i%15==0):
            collectData.append(f+' '+b)
        else:
            collectData.append(f)
    elif (i%5==0):
        collectData.append(b)
    else:
        collectData.append(i)
        
#print list/output
print (collectData)